package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IProjectRepository;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.Project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public String getIdByName(final String userId, final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName())) return project.getId();
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName()) && userId.equals(project.getName())) return true;
        }
        return false;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        remove(findOneByIndex(userId, index));
        return null;
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final String userId,@NotNull  final String name) {
        remove(findOneByName(userId, name));
        return null;
    }

}
