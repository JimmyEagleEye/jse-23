package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IUserRepository;
import ru.korkmasov.tsc.model.User;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return list.stream()
                .filter(e -> email.equals(e.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeUser(@NotNull final User user) {
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUserById(@NotNull final String id) {
        list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::removeUser);
        return null;
    }

    @Nullable
    @Override
    public User removeUserByLogin(@NotNull final String login) {
        list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

    @Override
    public boolean existsByLogin(@NotNull final String login) {
        return list.stream()
                .anyMatch(e -> login.equals(e.getLogin()));
    }

    public boolean existsByEmail(@NotNull final String email) {
        return list.stream()
                .anyMatch(e -> email.equals(e.getEmail()));
    }
}
