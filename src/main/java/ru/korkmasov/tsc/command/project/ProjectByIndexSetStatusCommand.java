package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.util.TerminalUtil;

import java.util.Arrays;

import static ru.korkmasov.tsc.util.ValidationUtil.checkIndex;

public class ProjectByIndexSetStatusCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-set-status-by-index";
    }

    @Override
    public String description() {
        return "Set project status by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!checkIndex(index, serviceLocator.getProjectService().size(userId))) throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getProjectService().changeProjectStatusByIndex(userId, index, Status.getStatus(TerminalUtil.nextLine()));
    }

}
