package ru.korkmasov.tsc.bootstrap;

import ru.korkmasov.tsc.api.repository.ICommandRepository;
import ru.korkmasov.tsc.api.repository.IProjectRepository;
import ru.korkmasov.tsc.api.repository.ITaskRepository;
import ru.korkmasov.tsc.api.repository.IUserRepository;
import ru.korkmasov.tsc.api.service.*;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.command.project.*;
import ru.korkmasov.tsc.command.system.*;
import ru.korkmasov.tsc.command.task.*;
import ru.korkmasov.tsc.command.user.*;
import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.system.UnknownCommandException;
import ru.korkmasov.tsc.repository.CommandRepository;
import ru.korkmasov.tsc.repository.ProjectRepository;
import ru.korkmasov.tsc.repository.TaskRepository;
import ru.korkmasov.tsc.repository.UserRepository;
import ru.korkmasov.tsc.service.*;
import ru.korkmasov.tsc.util.TerminalUtil;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);
    @NotNull
    private final IAuthService authService = new AuthService(userService);
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, authService);
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, authService);
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ProjectByIdFinishCommand());
        registry(new ProjectByIdRemoveCommand());
        registry(new ProjectByIdSetStatusCommand());
        registry(new ProjectByIdStartCommand());
        registry(new ProjectByIdUpdateCommand());
        registry(new ProjectByIdViewCommand());
        registry(new ProjectByIndexFinishCommand());
        registry(new ProjectByIndexRemoveCommand());
        registry(new ProjectByIndexSetStatusCommand());
        registry(new ProjectByIndexStartCommand());
        registry(new ProjectByIndexUpdateCommand());
        registry(new ProjectByIndexViewCommand());
        registry(new ProjectByNameFinishCommand());
        registry(new ProjectByNameRemoveCommand());
        registry(new ProjectByNameSetStatusCommand());
        registry(new ProjectByNameStartCommand());
        registry(new ProjectByNameUpdateCommand());
        registry(new ProjectByNameViewCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());

        registry(new TaskByIdFinishCommand());
        registry(new TaskByIdRemoveCommand());
        registry(new TaskByIdSetStatusCommand());
        registry(new TaskByIdStartCommand());
        registry(new TaskByIdUnbindCommand());
        registry(new TaskByIdUpdateCommand());
        registry(new TaskByIdViewCommand());
        registry(new TaskByIndexFinishCommand());
        registry(new TaskByIndexRemoveCommand());
        registry(new TaskByIndexSetStatusCommand());
        registry(new TaskByIndexStartCommand());
        registry(new TaskByIndexUpdateCommand());
        registry(new TaskByIndexViewCommand());
        registry(new TaskByNameFinishCommand());
        registry(new TaskByNameRemoveCommand());
        registry(new TaskByNameSetStatusCommand());
        registry(new TaskByNameStartCommand());
        registry(new TaskByNameUpdateCommand());
        registry(new TaskByNameViewCommand());
        registry(new TaskByNameViewCommand());
        registry(new TaskByProjectIdBindCommand());
        registry(new TaskByProjectIdListCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());

        registry(new AboutCommand());
        registry(new ArgumentsCommand());
        registry(new CommandsCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SysinfoCommand());
        registry(new VersionCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateCommand());
        registry(new UserViewProfileCommand());
        registry(new UserByLoginLockCommand());
        registry(new UserByLoginUnlockCommand());
        registry(new UserByLoginRemoveCommand());

    }

    public void run(@Nullable final String[] args) {
        loggerService.info("*** WeLcOmE To TaSk MaNaGeR ***");
        if (parseArgs(args)) System.exit(0);
        initData();
        while (true) {
            System.out.println("ENTER COMMAND:");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAILED]");
            }
        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        @NotNull final String idTest = userService.create("test", "test", "test@test.ru").getId();
        @NotNull final String idAdmin = userService.create("admin", "admin", Role.ADMIN).getId();

        projectService.add(idTest, "proj1", "desc1").setStatus(Status.COMPLETE);
        projectService.add(idTest, "proj2", "desc2").setStatus(Status.IN_PROGRESS);
        projectService.add(idTest, "proj3", "desc3").setStatus(Status.IN_PROGRESS);
        projectService.add(idAdmin, "proj4", "desc4").setStatus(Status.NOT_STARTED);
        projectService.add(idAdmin, "proj5", "desc5").setStatus(Status.COMPLETE);
        projectService.add(idAdmin, "proj6", "desc6").setStatus(Status.NOT_STARTED);

        taskService.add(idTest, "task1", "task_desc1").setStatus(Status.COMPLETE);
        taskService.add(idTest, "task2", "task_desc2").setStatus(Status.NOT_STARTED);
        taskService.add(idTest, "task3", "task_desc3").setStatus(Status.IN_PROGRESS);
        taskService.add(idAdmin, "task4", "task_desc4").setStatus(Status.NOT_STARTED);
        taskService.add(idAdmin, "task5", "task_desc5").setStatus(Status.IN_PROGRESS);
        taskService.add(idAdmin, "task6", "task_desc6").setStatus(Status.NOT_STARTED);
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }


    public void parseCommand(@Nullable final String com) {
        if (com == null) return;
        final AbstractCommand command = commandService.getCommandByName(com);
        if (command == null) throw new UnknownCommandException(com);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }


    @Override
    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public @NotNull ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return authService;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }
}