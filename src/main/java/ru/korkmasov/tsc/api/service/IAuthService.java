package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.model.User;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    @NotNull
    String getUserId();

    @Nullable
    User getUser();

    void checkRoles(@Nullable Role... roles);

    boolean isAuth();

    void logout();

    void login(@Nullable String login, String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
