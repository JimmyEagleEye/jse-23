package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.model.Task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {


    @Nullable
    List<Task> findAll(@NotNull String userId);

    List<Task> findAll(@NotNull String userId, Comparator<Task> comparator);

    @NotNull
    Task add(@NotNull String userId, @NotNull Task task);

    @NotNull
    Task add(@NotNull String userId, @NotNull String name, @Nullable String description);

    void remove(@NotNull String userId, @NotNull Task task);

    void clear(@NotNull String userId);

    void removeOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    Task findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    Task findOneByName(@NotNull String userId, @Nullable String name);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    void removeTaskByIndex(@NotNull String userId, @NotNull Integer index);

    Task findOneByIndex(@NotNull String userId,@Nullable Integer index);

    void updateTaskByIndex(String userId, Integer index, String name, String description);

    void updateTaskByName(@NotNull String userId, @NotNull String name, @NotNull String nameNew, @Nullable String description);

    void updateTaskById(@NotNull String userId, @NotNull String id, @NotNull String name, @Nullable String description);

    Task finishTaskById(@NotNull String userId, @Nullable String id);

    @Nullable
    Task finishTaskByName(@NotNull String userId, @Nullable String name);

    @Nullable
    Task startTaskById(@NotNull String userId, @Nullable String id);

    @Nullable
    Task startTaskByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task startTaskByName(@NotNull String userId, @Nullable String name);

    @Nullable
    Task changeTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    @Nullable
    Task changeTaskStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    @Nullable
    Task changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    int size(@NotNull String userId);

    boolean existsByName(@NotNull String userId, @NotNull String name);

}
