package ru.korkmasov.tsc.api.repository;

import ru.korkmasov.tsc.model.User;

import org.jetbrains.annotations.Nullable;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    User removeUser(User user);

    User removeUserById(@Nullable String id);

    User removeUserByLogin(@Nullable String login);

    boolean existsByLogin(@Nullable String login);

    boolean existsByEmail(@Nullable String email);

}
