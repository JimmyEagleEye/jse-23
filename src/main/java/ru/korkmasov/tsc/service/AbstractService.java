package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.IRepository;
import ru.korkmasov.tsc.api.service.IService;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.AbstractEntity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public int size() {
        return repository.size();
    }

    @Override
    public void add(@NotNull final E entity) {
        if (entity == null) return;
        repository.add(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Nullable
    public E findById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    public E removeById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
        return null;
    }


}
