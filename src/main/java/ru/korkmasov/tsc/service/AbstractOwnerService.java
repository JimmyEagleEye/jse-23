package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.IOwnerRepository;
import ru.korkmasov.tsc.api.service.IOwnerService;
import ru.korkmasov.tsc.api.service.IAuthService;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.AbstractOwner;
import ru.korkmasov.tsc.util.ValidationUtil;
import ru.korkmasov.tsc.model.Project;

import java.util.Comparator;
import java.util.List;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractOwnerService<E extends AbstractOwner> extends AbstractService<E> implements IOwnerService<E> {

    protected final IAuthService authService;

    public AbstractOwnerService(@NotNull final IOwnerRepository<E> repository, IAuthService authService) {
        super(repository);
        this.authService = authService;
        this.repository = repository;
    }

    protected final IOwnerRepository<E> repository;

    @NotNull
    public List<E> findAll(final @NotNull String userId) {
        return repository.findAll(userId);
    }

    @NotNull
    public List<E> findAll(final @NotNull String userId, final Comparator<E> comparator) {
        return repository.findAll(userId, comparator);
    }

    public E findById(@NotNull final String userId,@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public int size(@NotNull final String userId) {
        if (repository.size() == 0) return 0;
        return repository.size(userId);
    }

    public boolean existsById(@NotNull final String userId,@NotNull final String id) {
        if (ValidationUtil.isEmpty(id)) throw new EmptyIdException();
        return repository.existsById(userId, id);
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @Override
    public Project removeById(@NotNull final String userId,@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(userId, id);
        return null;
    }

}
