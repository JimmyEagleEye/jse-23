package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.IProjectRepository;
import ru.korkmasov.tsc.api.service.IProjectTaskService;
import ru.korkmasov.tsc.api.repository.ITaskRepository;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.util.ValidationUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public List<Task> findALLTaskByProjectId(@NotNull final String userId,@NotNull final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.findALLTaskByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task assignTaskByProjectId(@NotNull final String userId, @NotNull final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.assignTaskByProjectId(userId, projectId, taskId);
    }

    @Nullable
    @Override
    public Task unassignTaskByProjectId(@NotNull final String userId,@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.unassignTaskByProjectId(userId, taskId);
    }

    @Nullable
    @Override
    public List<Task> removeTasksByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.removeAllTaskByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Project removeProjectById(@NotNull final String userId, @NotNull final String projectId) {
        if (ValidationUtil.isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeById(userId, projectId);
        return null;
    }

    @Override
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        if (ValidationUtil.isEmpty(projectName)) throw new EmptyNameException();
        @NotNull String projectId = projectRepository.getIdByName(userId,projectName);
        if (ValidationUtil.isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeOneByName(userId, projectName);
    }


}
