package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.IProjectRepository;
import ru.korkmasov.tsc.api.service.IAuthService;
import ru.korkmasov.tsc.api.service.IProjectService;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.util.ValidationUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository, IAuthService authService) {
        super(projectRepository, authService);
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (ValidationUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneById(@NotNull final String userId,@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(userId, id);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId,@NotNull final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final String userId, @NotNull final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Nullable
    @Override
    public Project removeProjectByIndex(@NotNull final String userId,@NotNull final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId,@NotNull final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > projectRepository.size(userId)) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project updateProjectByIndex(@NotNull final String userId,@NotNull final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = projectRepository.findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project updateProjectById(@NotNull final String userId,@NotNull final String id,@NotNull final String name, @NotNull final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project updateProjectByName(@NotNull final String userId,@NotNull final String name, @NotNull final String nameNew,@NotNull final String description) {
        if (ValidationUtil.isEmpty(name) || ValidationUtil.isEmpty(nameNew)) throw new EmptyNameException();
        @NotNull final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(nameNew);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project startProjectById(@NotNull String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = projectRepository.findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @NotNull
    @Override
    public Project startProjectByIndex(@NotNull final String userId,@NotNull final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;

    }

    @Nullable
    @Override
    public Project startProjectByName(@NotNull String userId,@Nullable String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishProjectById(@NotNull final String userId, @NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishProjectByIndex(@NotNull  final String userId,@Nullable  Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishProjectByName(@NotNull final String userId,@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusById(@NotNull final String userId,@Nullable final String id,@Nullable final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusByName(@NotNull final String userId, @Nullable final String name, @NotNull final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull Project project = findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusByIndex(@NotNull final String userId,@Nullable final Integer index,@NotNull final Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final @NotNull Project project = findOneByIndex(userId,index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public boolean existsByName(@NotNull String userId,@Nullable String name) {
        if (ValidationUtil.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.existsByName(userId, name);
    }

}

