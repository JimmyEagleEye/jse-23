package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.ITaskRepository;
import ru.korkmasov.tsc.api.service.IAuthService;
import ru.korkmasov.tsc.api.service.ITaskService;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.util.ValidationUtil;

import java.util.Objects;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository, IAuthService authService) {
        super(taskRepository, authService);
        this.taskRepository = taskRepository;
    }

    @Override
    public @NotNull Task add(final @NotNull String userId, final @Nullable Task task) {
        if (task == null) return null;
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public @NotNull Task add(final String userId, final String name, final String description) {
        if (ValidationUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void remove(final @NotNull String userId, final Task task) {
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @Override
    public void removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.removeOneById(userId, id);
    }

    @Override
    public @Nullable Task findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Override
    public @Nullable Task findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public void removeOneByName(String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.removeOneByName(userId, name);
    }

    @Override
    public void removeTaskByIndex(@NotNull String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task findOneByIndex(@NotNull String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > taskRepository.size(userId)) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public void updateTaskByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final String name,
            @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void updateTaskByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String nameNew,
            @Nullable final String description
    ) {
        if (ValidationUtil.isEmpty(name) || ValidationUtil.isEmpty(nameNew)) throw new EmptyNameException();
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setName(nameNew);
        task.setDescription(description);
    }

    @Override
    public void updateTaskById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (ValidationUtil.isEmpty(id)) throw new EmptyIdException();
        if (ValidationUtil.isEmpty(name)) throw new EmptyNameException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public Task finishTaskById(@NotNull String userId, @NotNull String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task finishTaskByName(@NotNull String userId, @Nullable String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task startTaskById(@NotNull String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final @NotNull Task task = Objects.requireNonNull(findOneById(userId, id));
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(@NotNull String userId, @Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task startTaskByName(@NotNull String userId, @Nullable String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task changeTaskStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(String userId, String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(@NotNull String userId, Integer index, @NotNull Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public String getIdByIndex(@NotNull String userId, @NotNull Integer index) {
        if (!ValidationUtil.checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        return taskRepository.getIdByIndex(index);
    }

    public boolean existsByName(@NotNull String userId,@Nullable String name) {
        if (ValidationUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.existsByName(userId, name);
    }


}
