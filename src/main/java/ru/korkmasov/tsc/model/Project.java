package ru.korkmasov.tsc.model;

import ru.korkmasov.tsc.api.entity.ITWBS;
import ru.korkmasov.tsc.enumerated.Status;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractOwner implements ITWBS {

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date created = new Date();

    public Project(@Nullable String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

    public Project(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getId() + " :" + name;
    }

}
